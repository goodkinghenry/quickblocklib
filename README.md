This is a work-in-progress library meant to ease the development of Minecraft Spigot plugins. Code repetition can be reduced by using the functions, switches, definitions, etc. within; I have not yet documented this library, but feel free to use and peruse it as you see fit.

We are currently on Spigot API 1.19.

Current version: 0.4-SNAPSHOT

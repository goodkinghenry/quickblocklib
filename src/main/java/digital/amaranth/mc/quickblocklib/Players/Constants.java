/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Players;

/**
 * Constants pertaining to Players.
 * @author goodkinghenry
 */
public class Constants {
    /**
     * the max natural value for a player's food level;
     */
    public static final int MAX_FOOD_LEVEL = 20;
}

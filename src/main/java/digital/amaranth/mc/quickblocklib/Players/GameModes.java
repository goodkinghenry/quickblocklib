/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Players;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

/**
 * Methods for checking on the current GameMode of a Player.
 * @author goodkinghenry
 */
public class GameModes {
    public static boolean isInSurvivalMode (Player p) {
        return p.getGameMode().equals(GameMode.SURVIVAL);
    }
    
    public static boolean isInCreativeMode (Player p) {
        return p.getGameMode().equals(GameMode.CREATIVE);
    }
    
    public static boolean isInAdventureMode (Player p) {
        return p.getGameMode().equals(GameMode.ADVENTURE);
    }
    
    public static boolean isInSpectatorMode (Player p) {
        return p.getGameMode().equals(GameMode.SPECTATOR);
    }
}

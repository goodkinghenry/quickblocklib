/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Plugins;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Convenience methods for getting and modifying Plugin data.
 * @author goodkinghenry
 */
public class Plugins {
    /**
     * @param pluginName the name of the plugin
     * @return a JavaPlugin by name; throws a run-time exception if the plugin is not found
     * @author Unknown Contributor
     */
    public static JavaPlugin getInstanceByName(String pluginName) {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin(pluginName);
        
        if (plugin == null || !(plugin instanceof JavaPlugin)) {
            throw new RuntimeException("'"+pluginName+"' not found.");
        }
        
        return ((JavaPlugin) plugin);
    }
}

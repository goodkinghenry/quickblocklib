/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Biomes;

import org.bukkit.block.Block;

/**
 * Switches for matching Blocks into Biome categories.
 * @author goodkinghenry
 */
public class Categories {
    /**
     * @param b
     * @return true if the biome containing b naturally generates acacia trees
     */
    public static boolean isAnAcaciaGeneratingBiome (Block b) {
        return switch (b.getBiome()) {
            case SAVANNA -> true;
            case SAVANNA_PLATEAU -> true;
            default -> false;
        };
    }
    
    /**
     * @param b
     * @return true if the biome containing b naturally generates oak trees
     */
    public static boolean isAnOakGeneratingBiome (Block b) {
        return switch (b.getBiome()) {
            case BAMBOO_JUNGLE -> true;
            case DARK_FOREST -> true;
            case FOREST -> true;
            case JUNGLE -> true;
            case PLAINS -> true;
            case RIVER -> true;
            case SAVANNA -> true;
            case SWAMP -> true;
            default -> false;
        };
    }
    
    /**
     * @param b
     * @return true if b is in a swamp biome
     */
    public static boolean isInASwamp (Block b) {
        return switch (b.getBiome()) {
            case SWAMP -> true;
            default -> false;
        };
    }
    
    /**
     * @param b
     * @return true if rain does not naturally occur in the biome containing b
     */
    public static boolean isInARainlessBiome (Block b) {
        return switch (b.getBiome()) {
            case DESERT -> true;
            case SAVANNA -> true;
            case SAVANNA_PLATEAU -> true;
            case BADLANDS -> true;
            default -> false;
        };
    }
    
    /**
     * @param b
     * @return true if b is in a biome dominated by freshwater.
     */
    public static boolean isInAFreshwaterBiome (Block b) {
        return switch (b.getBiome()) {
            case RIVER -> true;
            case SWAMP -> true;
            case FROZEN_RIVER -> true;
            default -> false;
        };
    }
    
    /**
     * @param b
     * @return true if b is in an ocean biome.
     */
    public static boolean isInAnOceanBiome (Block b) {
        return switch (b.getBiome()) {
            case BEACH -> true;
            case OCEAN -> true;
            case COLD_OCEAN -> true;
            case DEEP_COLD_OCEAN -> true;
            case DEEP_LUKEWARM_OCEAN -> true;
            case DEEP_OCEAN -> true;
            case LUKEWARM_OCEAN -> true;
            case SNOWY_BEACH -> true;
            case WARM_OCEAN -> true;
            case FROZEN_OCEAN -> true;
            default -> false;
        };
    }
    
    /**
     * @param b
     * @return true if b is in a biome dominated by water.
     */
    public static boolean isInAWaterBiome (Block b) {
        return isInAnOceanBiome(b) || isInAFreshwaterBiome(b);
    }
}

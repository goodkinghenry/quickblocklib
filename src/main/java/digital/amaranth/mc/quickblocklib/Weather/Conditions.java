/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Weather;

import digital.amaranth.mc.quickblocklib.Biomes.Categories;

import org.bukkit.World;

import org.bukkit.block.Block;

/**
 * Convenience methods for checking a Block's weather/climactic conditions.
 * @author goodkinghenry
 */
public class Conditions {
    /**
     * @param b
     * @return true if b's temperature is below the game's freezing temperature
     */
    public static boolean isFreezing (Block b) {
        return b.getTemperature() <= Constants.MAX_SNOW_TEMPERATURE;
    }
    
    public static boolean hasLightFromSky (Block b) {
        return b.getLightFromSky() != 0;
    }
    
    /**
     * @param b
     * @return true if b is the highest block at its horizontal coordinates
     */
    public static boolean hasSkyAbove (Block b) {
        return b.getWorld().getHighestBlockAt(b.getLocation()).equals(b);
    }
    
    /**
     * @param b
     * @return true if b is unsheltered and is in a biome that receives rainfall
     */
    public static boolean canReceiveRainfall (Block b) {
        return hasSkyAbove(b) && !Categories.isInARainlessBiome(b);
    }
    
    /**
     * @param b
     * @return true if b is unsheltered during a weather event and rain, not snow, will fall
     */
    public static boolean isInRain (Block b) {
        return hasWeather(b) && !isFreezing(b);
    }
    
    /**
     * @param b
     * @return true if b is unsheltered during a weather event and snow, not rain, will fall
     */
    public static boolean isInSnow (Block b) {
        return hasWeather(b) && isFreezing(b);
    }
    
    /**
     * @param b
     * @return true if b is unsheltered and a thunderstorm is occurring
     */
    public static boolean isInAStorm (Block b) {
        return canReceiveRainfall(b) && b.getWorld().isThundering();
    }
    
    /**
     * @param b
     * @return true if b is unsheltered where a thunderstorm is occurring and snow, not rain, will fall
     */
    public static boolean isInASnowstorm (Block b) {
        return isInAStorm(b) && isFreezing(b);
    }
    
    /**
     * @param b
     * @return true if b is unsheltered where a thunderstorm is occurring and rain, not snow, will fall
     */
    public static boolean isInAThunderstorm (Block b) {
        return isInAStorm(b) && !isFreezing(b);
    }
    
    /**
     * @param b
     * @return true if b is unsheltered and a weather event is currently underway
     */
    public static boolean hasWeather (Block b) {
        return canReceiveRainfall(b) && !b.getWorld().isClearWeather();
    }
    
    /**
     * slapdash function for determining if it's nighttime.
     * @param block
     * @return true if it's nighttime
     */
    public static boolean isNightTime (Block block) {
        World world = block.getWorld();
        
        return Constants.NIGHT_START_TICKS < world.getTime() && world.getTime() < Constants.NIGHT_END_TICKS;
    }
    
    /**
     * @param block
     * @return true if it's not nighttime.
     */
    public static boolean isDayTime (Block block) {
        return !isNightTime(block);
    }
    
    /**
     * @param b
     * @return true if it's daytime and the weather is clear
     */
    public static boolean hasSunlight (Block b) {
        return isDayTime(b) && b.getWorld().isClearWeather();
    }
    
    /**
     * @param b
     * @return light level from the sky if there is clear weather and it's daytime, or 0 otherwise
     */
    public static int getSunlight (Block b) {
        if (hasSunlight(b)) {
            return b.getLightFromSky();
        } else {
            return 0;
        }
    }
    
    /**
     * slapdash function for calculating the apparent position of the sun in the sky.
     * @param b
     * @return
     */
    public static double getSunHeightInSky (Block b) {
        long adjustedTime = b.getWorld().getTime() + 1000;
        
        if (0 < adjustedTime && adjustedTime > Constants.NIGHT_START_TICKS + 1000) {
            if (adjustedTime <= Constants.NOON_TICKS + 1000) {
                return 2 * adjustedTime / Constants.DAY_LENGTH_TICKS;
            } else {
                return 2 * (1 - adjustedTime / Constants.DAY_LENGTH_TICKS);
            }
        } else {
            return 0;
        }
    }
}

/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Weather;

/**
 * Constants pertaining to time, weather, and climate.
 * @author goodkinghenry
 */
public class Constants {
    /**
     * this is the game's built-in maximum temperature at which snow will fall
     */
    public static final double MAX_SNOW_TEMPERATURE = 0.15;
    
    /**
     * world tick at which nighttime starts
     */
    
    public static final int NIGHT_START_TICKS = 13000;
    /**
     * world tick at which daytime starts
     */
    
    public static final int NIGHT_END_TICKS = 23000;

    /**
     * world tick at which noon occurs
     */
    public static final int NOON_TICKS = 7000;

    /**
     * number of ticks that daytime lasts
     */
    public static final int DAY_LENGTH_TICKS = 14000;
    
    /**
     * number of ticks that nighttime lasts
     */
    public static final int NIGHT_LENGTH_TICKS = 10000;
}

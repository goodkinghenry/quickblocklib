/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Neighbors;

import java.util.Arrays;
import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

/**
 * Convenience methods for retrieving Lists of neighboring Blocks.
 * @author goodkinghenry
 */
public class Neighbors {
    public static List<Block> getNeighbors (Block b, BlockFace[] faceList) {
        return Arrays.asList(faceList).stream().map(face -> b.getRelative(face)).toList();
    }
    
    public static List<Block> getDirectAndDiagonalNeighbors (Block b) {
        return getNeighbors(b, Groups.ALL_DIRECT_FACES_AND_DIAGONALS);
    }
    
    public static List<Block> getDirectNeighbors (Block b) {
        return getNeighbors(b, Groups.ALL_DIRECT_FACES);
    }
    
    /**
     * @param b
     * @return a list of b's direct compass-direction neighbors: east, north, west, and south
     */
    public static List<Block> getCardinalNeighbors (Block b) {
        return getNeighbors(b, Groups.ALL_CARDINAL_FACES);
    }
    
    public static List<Block> getDirectNeighborsExceptUp (Block b) {
        return getNeighbors(b, Groups.ALL_DIRECT_FACES_EXCEPT_UP);
    }
    
    public static List<Block> getDirectNeighborsExceptDown (Block b) {
        return getNeighbors(b, Groups.ALL_DIRECT_FACES_EXCEPT_DOWN);
    }
}

/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Neighbors;

import org.bukkit.block.BlockFace;

/**
 * Predefined arrays[] of BlockFaces, for use in methods which iterate over a Block's neighbors.
 * @author goodkinghenry
 */
public class Groups {
    public static final BlockFace[] ALL_DIRECT_FACES_AND_DIAGONALS = {
        BlockFace.DOWN,
        BlockFace.EAST,
        BlockFace.NORTH_EAST,
        BlockFace.NORTH,
        BlockFace.NORTH_WEST,
        BlockFace.WEST,
        BlockFace.SOUTH_EAST,
        BlockFace.SOUTH,
        BlockFace.SOUTH_WEST,
        BlockFace.UP,
    };
    
    public static final BlockFace[] ALL_DIRECT_FACES = {
        BlockFace.DOWN,
        BlockFace.EAST,
        BlockFace.NORTH,
        BlockFace.WEST,
        BlockFace.SOUTH,
        BlockFace.UP,
    };
    
    public static final BlockFace[] ALL_CARDINAL_FACES = {
        BlockFace.EAST,
        BlockFace.SOUTH,
        BlockFace.WEST,
        BlockFace.NORTH,
    };
    
    public static final BlockFace[] ALL_DIRECT_FACES_EXCEPT_UP = {
        BlockFace.DOWN,
        BlockFace.EAST,
        BlockFace.NORTH,
        BlockFace.WEST,
        BlockFace.SOUTH,
    };
    
    public static final BlockFace[] ALL_DIRECT_FACES_EXCEPT_DOWN = {
        BlockFace.UP,
        BlockFace.EAST,
        BlockFace.NORTH,
        BlockFace.WEST,
        BlockFace.SOUTH,
    };
}

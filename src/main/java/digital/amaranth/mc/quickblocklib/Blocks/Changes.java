/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Blocks;

import org.bukkit.Material;

import org.bukkit.block.Block;

/**
 * Convenient functions for altering Blocks.
 * @author goodkinghenry
 */
public class Changes {
    /**
     * @param b Block to be broken and changed
     * @param t Material to which b will be changed
     */
    public static void breakAndSetType (Block b, Material t) {
        b.breakNaturally();
        b.setType(t);
    }
    
    /**
     * @param to Block to be broken and replaced
     * @param from Block whose type will be copied
     */
    public static void copyTo (Block to, Block from) {
        breakAndSetType(to, from.getType());
    }
    
    /**
     * @param b Block to be replaced with air
     */
    public static void removeBlock (Block b) {
        b.setType(Material.AIR);
    }
    
    /**
     * @param to Block to be broken and transferred to
     * @param from Block to be transferred
     */
    public static void transferTo (Block to, Block from) {
        copyTo(to, from);
        removeBlock(from);
    }
    
    /**
     * @param b will be made to fall, as a sand Block would
     */
    public static void applyGravityTo (Block b) {
        b.getWorld().spawnFallingBlock(b.getLocation().add(0.5, 0, 0.5), b.getBlockData());
        
        removeBlock(b);
    }
}

/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Blocks;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;

/**
 * This abstract class allows for a scheduled, repeating action upon an entire list of Blocks,
 * defined by overriding the method 'action(*)'; a pair of Lists are used, one of which is written to
 * while the other is acted upon
 * @author goodkinghenry
 */
public abstract class BulkActionList {
    private final List<Block> la, lb;

    private int taskID;
    private final Plugin p;
    private final long t;
    
    private boolean toggle;
    
    private void process () {
        if (toggle) {
            action(lb);
            
            lb.clear();
        } else {
            action(la);
            
            la.clear();
        }
        
        toggle = !toggle;
    }
    
    /**
     * this method describes what will be done to the active list each cycle
     * @param l the list to be acted upon
     */
    protected abstract void action (List<Block> l);
    
    /**
     * start the ActionQueue's repeating schedule
     * @param d number of ticks to delay before starting
     */
    public final void start (long d) {
        if (!Bukkit.getServer().getScheduler().isCurrentlyRunning(taskID)) {
            taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(p, () -> {
                process();
            }, d, t);
        } else {
            stop();
            start(d);
        }
    }
    
    /**
     * stop the ActionQueue's repeating schedule
     */
    public final void stop () {
        Bukkit.getServer().getScheduler().cancelTask(taskID);
        
        taskID = -1;
    }
    
    /**
     * @param b the Block which is to be processed at the next cycle
     */
    public final void addBlock (Block b) {
        if (toggle) {
            if (!la.contains(b)) {
                la.add(b);
            }
        } else {
            if (!lb.contains(b)) {
                lb.add(b);
            }
        }
    }
    
    /**
     * @param b the Block which is to be removed from the BulkActionList for this cycle
     */
    public final void removeBlock (Block b) {
        if (toggle) {
            la.remove(b);
        } else {
            lb.remove(b);
        }
    }
    
    /**
     * @return the actively-written List of Blocks
     */
    public final List<Block> getBlocks () {
        if (toggle) {
            return la;
        } else {
            return lb;
        }
    }
    
    /**
     * @param t the number of ticks per cycle
     * @param d the number of ticks to wait before starting
     * @param p the Plugin that owns this BulkActionList
     */
    public BulkActionList(
            long t,
            long d,
            Plugin p
    ) {
        la = new ArrayList<>();
        lb = new ArrayList<>();
        
        this.t = t;
        this.p = p;
        
        this.toggle = false;
        
        start(d);
    }
}

/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Blocks;

import org.bukkit.Material;
import org.bukkit.TreeType;

/**
 * Switches for matching Blocks to specific other types according to their Material Type.
 * @author goodkinghenry
 */
public class Switches {    
    /**
     * @param t
     * @return the sapling associated with t, or air if no match found
     */
    public static Material getSaplingByTreeType (TreeType t) {
        return switch (t) {
            case ACACIA -> Material.ACACIA_SAPLING;
            case AZALEA -> Material.OAK_SAPLING;
            case BIG_TREE -> Material.OAK_SAPLING;
            case BIRCH -> Material.BIRCH_SAPLING;
            case BROWN_MUSHROOM -> Material.BROWN_MUSHROOM;
            case CHORUS_PLANT -> Material.CHORUS_PLANT;
            case COCOA_TREE -> Material.JUNGLE_SAPLING;
            case CRIMSON_FUNGUS -> Material.CRIMSON_FUNGUS;
            case DARK_OAK -> Material.DARK_OAK_SAPLING;
            case JUNGLE -> Material.JUNGLE_SAPLING;
            case JUNGLE_BUSH -> Material.JUNGLE_SAPLING;
            case MANGROVE -> Material.MANGROVE_PROPAGULE;
            case MEGA_REDWOOD -> Material.SPRUCE_SAPLING;
            case RED_MUSHROOM -> Material.RED_MUSHROOM;
            case REDWOOD -> Material.SPRUCE_SAPLING;
            case SMALL_JUNGLE -> Material.JUNGLE_SAPLING;
            case SWAMP -> Material.OAK_SAPLING;
            case TALL_BIRCH -> Material.BIRCH_SAPLING;
            case TALL_MANGROVE -> Material.MANGROVE_PROPAGULE;
            case TALL_REDWOOD -> Material.SPRUCE_SAPLING;
            case TREE -> Material.OAK_SAPLING;
            case WARPED_FUNGUS -> Material.WARPED_FUNGUS;
            default -> Material.AIR;
        };
    }
}

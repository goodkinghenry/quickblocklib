/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Blocks;

import org.bukkit.block.Block;

/**
 * Neat little methods for checking on simple Block conditions.
 * @author goodkinghenry
 */
public class Conditions {
    /**
     * @param a
     * @param b
     * @return true if a and b are of the same material type
     */
    public static boolean areSameType (Block a, Block b) {
        return a.getType().equals(b.getType());
    }
    
    /**
     * @param a
     * @param b
     * @return true if block a is vertically higher than block b
     */
    public static boolean isHigherThan (Block a, Block b) {
        return a.getY() > b.getY();
    }
    
    /**
     * @param a
     * @param b
     * @return true if block a is vertically lower than block b
     */
    public static boolean isLowerThan (Block a, Block b) {
        return a.getY() < b.getY();
    }
    
    /**
     *
     * @param b
     * @return true if a block is the highest one at its x and z coordinates
     */
    public static boolean isHighest (Block b) {
        return b.getWorld().getHighestBlockAt(b.getLocation()).getY() <= b.getY();
    }
    
    public static boolean isSolid (Block b) {
        return b.getType().isSolid();
    }
}

/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Blocks;

import org.bukkit.Material;
import org.bukkit.block.Block;

/**
 * Switches for matching Blocks into categories according to their Material Type.
 * @author goodkinghenry
 */
public class Categories {
    public static boolean isABoat (Material m) {
        return switch (m) {
            case ACACIA_BOAT -> true;
            case ACACIA_CHEST_BOAT -> true;
            case BIRCH_BOAT -> true;
            case BIRCH_CHEST_BOAT -> true;
            case DARK_OAK_BOAT -> true;
            case DARK_OAK_CHEST_BOAT -> true;
            case JUNGLE_BOAT -> true;
            case JUNGLE_CHEST_BOAT -> true;
            case MANGROVE_BOAT -> true;
            case MANGROVE_CHEST_BOAT -> true;
            case OAK_BOAT -> true;
            case OAK_CHEST_BOAT -> true;
            case SPRUCE_BOAT -> true;
            case SPRUCE_CHEST_BOAT -> true;
            default -> false;
        };
    }
    
    public static boolean isRock (Block b) {
        return switch (b.getType()) {
            case STONE -> true;
            case DEEPSLATE -> true;
            case ANDESITE -> true;
            case GRANITE -> true;
            case DIORITE -> true;
            default -> false;
        };
    }
    public static boolean isOre (Block b) {
        return switch (b.getType()) {
            case COAL_ORE -> true;
            case COPPER_ORE -> true;
            case DEEPSLATE_COAL_ORE -> true;
            case DEEPSLATE_COPPER_ORE -> true;
            case DEEPSLATE_DIAMOND_ORE -> true;
            case DEEPSLATE_EMERALD_ORE -> true;
            case DEEPSLATE_GOLD_ORE -> true;
            case DEEPSLATE_IRON_ORE -> true;
            case DEEPSLATE_LAPIS_ORE -> true;
            case DEEPSLATE_REDSTONE_ORE -> true;
            case DIAMOND_ORE -> true;
            case EMERALD_ORE -> true;
            case GOLD_ORE -> true;
            case IRON_ORE -> true;
            case LAPIS_ORE -> true;
            case REDSTONE_ORE -> true;
            case NETHER_GOLD_ORE -> true;
            case NETHER_QUARTZ_ORE -> true;
            default -> false;
        };
    }
    
    public static boolean isSoil (Block b) {
        return switch (b.getType()) {
            case DIRT -> true;
            case GRASS_BLOCK -> true;
            case COARSE_DIRT -> true;
            case PODZOL -> true;
            case MYCELIUM -> true;
            case ROOTED_DIRT -> true;
            case MOSS_BLOCK -> true;
            case FARMLAND -> true;
            case MUD -> true;
            case MUDDY_MANGROVE_ROOTS -> true;
            case CLAY -> true;
            default -> false;
        };
    }
    
    public static boolean isAquaticSoil (Block b) {
        return switch (b.getType()) {
            case SAND -> true;
            case GRAVEL -> true;
            case CLAY -> true;
            case MUD -> true;
            case DIRT -> true;
            case MUDDY_MANGROVE_ROOTS -> true;
            default -> false;
        };
    }
    
    /**
     * @param b any Block
     * @return true if b is of a type that naturally falls when unsupported
     */
    public static boolean canFall (Block b) {
        return switch (b.getType()) {
            case SAND -> true;
            case GRAVEL -> true;
            case RED_SAND -> true;
            case LIGHT_BLUE_CONCRETE_POWDER -> true;
            case BLUE_CONCRETE_POWDER -> true;
            case BROWN_CONCRETE_POWDER -> true;
            case CYAN_CONCRETE_POWDER -> true;
            case GRAY_CONCRETE_POWDER -> true;
            case GREEN_CONCRETE_POWDER -> true;
            case LIGHT_GRAY_CONCRETE_POWDER -> true;
            case LIME_CONCRETE_POWDER -> true;
            case MAGENTA_CONCRETE_POWDER -> true;
            case ORANGE_CONCRETE_POWDER -> true;
            case PINK_CONCRETE_POWDER -> true;
            case PURPLE_CONCRETE_POWDER -> true;
            case RED_CONCRETE_POWDER -> true;
            case WHITE_CONCRETE_POWDER -> true;
            case YELLOW_CONCRETE_POWDER -> true;
            case BLACK_CONCRETE_POWDER -> true;
            case SOUL_SAND -> true;
            default -> false;
        };
    }
    
    public static boolean isLeaves (Block b) {
        return switch (b.getType()) {
            case ACACIA_LEAVES -> true;
            case AZALEA_LEAVES -> true;
            case BIRCH_LEAVES -> true;
            case DARK_OAK_LEAVES -> true;
            case FLOWERING_AZALEA_LEAVES -> true;
            case JUNGLE_LEAVES -> true;
            case OAK_LEAVES -> true;
            case SPRUCE_LEAVES -> true;
            case MANGROVE_LEAVES -> true;
            default -> false;
        };
    }
    
    public static boolean isGlass (Block b) {
        return switch (b.getType()) {
            case GLASS -> true;
            case GLASS_PANE -> true;
            case BLACK_STAINED_GLASS -> true;
            case BLACK_STAINED_GLASS_PANE -> true;
            case BLUE_STAINED_GLASS -> true;
            case BLUE_STAINED_GLASS_PANE -> true;
            case BROWN_STAINED_GLASS -> true;
            case BROWN_STAINED_GLASS_PANE -> true;
            case CYAN_STAINED_GLASS -> true;
            case CYAN_STAINED_GLASS_PANE -> true;
            case GRAY_STAINED_GLASS -> true;
            case GRAY_STAINED_GLASS_PANE -> true;
            case GREEN_STAINED_GLASS -> true;
            case GREEN_STAINED_GLASS_PANE -> true;
            case LIGHT_BLUE_STAINED_GLASS -> true;
            case LIGHT_BLUE_STAINED_GLASS_PANE -> true;
            case LIGHT_GRAY_STAINED_GLASS -> true;
            case LIGHT_GRAY_STAINED_GLASS_PANE -> true;
            case LIME_STAINED_GLASS -> true;
            case LIME_STAINED_GLASS_PANE -> true;
            case MAGENTA_STAINED_GLASS -> true;
            case MAGENTA_STAINED_GLASS_PANE -> true;
            case ORANGE_STAINED_GLASS -> true;
            case ORANGE_STAINED_GLASS_PANE -> true;
            case PINK_STAINED_GLASS -> true;
            case PINK_STAINED_GLASS_PANE -> true;
            case PURPLE_STAINED_GLASS -> true;
            case PURPLE_STAINED_GLASS_PANE -> true;
            case RED_STAINED_GLASS -> true;
            case RED_STAINED_GLASS_PANE -> true;
            case TINTED_GLASS -> true;
            case WHITE_STAINED_GLASS -> true;
            case WHITE_STAINED_GLASS_PANE -> true;
            case YELLOW_STAINED_GLASS -> true;
            case YELLOW_STAINED_GLASS_PANE -> true;
            default -> false;
        };
    }
    
    public static boolean isCobbled (Block b) {
        return switch (b.getType()) {
            case COBBLESTONE -> true;
            case COBBLESTONE_SLAB -> true;
            case COBBLESTONE_STAIRS -> true;
            case COBBLESTONE_WALL -> true;
            case COBBLED_DEEPSLATE -> true;
            case COBBLED_DEEPSLATE_SLAB -> true;
            case COBBLED_DEEPSLATE_STAIRS -> true;
            case COBBLED_DEEPSLATE_WALL -> true;
            case MOSSY_COBBLESTONE -> true;
            case MOSSY_COBBLESTONE_SLAB -> true;
            case MOSSY_COBBLESTONE_STAIRS -> true;
            case MOSSY_COBBLESTONE_WALL -> true;
            case INFESTED_COBBLESTONE -> true;
            default -> false;
        };
    }
    
    public static boolean isPlanks (Block b) {
        return switch (b.getType()) {
            case ACACIA_PLANKS -> true;
            case BIRCH_PLANKS -> true;
            case CRIMSON_PLANKS -> true;
            case DARK_OAK_PLANKS -> true;
            case JUNGLE_PLANKS -> true;
            case OAK_PLANKS -> true;
            case SPRUCE_PLANKS -> true;
            case WARPED_PLANKS -> true;
            case BAMBOO_PLANKS -> true;
            case MANGROVE_PLANKS -> true;
            default -> false;
        };
    }
    
    /**
     * @param b any Block
     * @return true if it is a coral, kelp, or sea grass
     */
    public static boolean isAnAquaticPlant (Block b) {
        return switch (b.getType()) {
            case KELP -> true;
            case KELP_PLANT -> true;
            case SEAGRASS -> true;
            case TALL_SEAGRASS -> true;
            case BRAIN_CORAL -> true;
            case BRAIN_CORAL_FAN -> true;
            case BRAIN_CORAL_WALL_FAN -> true;
            case BUBBLE_CORAL -> true;
            case BUBBLE_CORAL_FAN -> true;
            case BUBBLE_CORAL_WALL_FAN -> true;
            case DEAD_BRAIN_CORAL -> true;
            case DEAD_BRAIN_CORAL_FAN -> true;
            case DEAD_BRAIN_CORAL_WALL_FAN -> true;
            case DEAD_BUBBLE_CORAL -> true;
            case DEAD_BUBBLE_CORAL_FAN -> true;
            case DEAD_BUBBLE_CORAL_WALL_FAN -> true;
            case DEAD_FIRE_CORAL -> true;
            case DEAD_FIRE_CORAL_FAN -> true;
            case DEAD_FIRE_CORAL_WALL_FAN -> true;
            case DEAD_HORN_CORAL -> true;
            case DEAD_HORN_CORAL_FAN -> true;
            case DEAD_HORN_CORAL_WALL_FAN -> true;
            case DEAD_TUBE_CORAL -> true;
            case DEAD_TUBE_CORAL_FAN -> true;
            case DEAD_TUBE_CORAL_WALL_FAN -> true;
            case FIRE_CORAL -> true;
            case FIRE_CORAL_FAN -> true;
            case FIRE_CORAL_WALL_FAN -> true;
            case HORN_CORAL -> true;
            case HORN_CORAL_FAN -> true;
            case HORN_CORAL_WALL_FAN -> true;
            case TUBE_CORAL -> true;
            case TUBE_CORAL_FAN -> true;
            case TUBE_CORAL_WALL_FAN -> true;
            default -> false;
        };
    }
    
    public static boolean isABush (Block b) {
        return switch (b.getType()) {
            case SWEET_BERRY_BUSH -> true;
            case PUMPKIN_STEM -> true;
            case MELON_STEM -> true;
            default -> false;
        };
    }
    
    /**
     * @param b any block
     * @return true if b is a sapling of a tree
     */
    public static boolean isATreeSapling (Block b) {
        return switch (b.getType()) {
            case ACACIA_SAPLING -> true;
            case BIRCH_SAPLING -> true;
            case DARK_OAK_SAPLING -> true;
            case JUNGLE_SAPLING -> true;
            case OAK_SAPLING -> true;
            case SPRUCE_SAPLING -> true;
            case MANGROVE_PROPAGULE -> true;
            default -> false;
        };
    }
    
    /**
     *
     * @param b any block
     * @return true if b is a plantable-type block
     */
    public static boolean isAPlant (Block b) {
        return switch (b.getType()) {
            case ACACIA_SAPLING -> true;
            case BIRCH_SAPLING -> true;
            case DARK_OAK_SAPLING -> true;
            case JUNGLE_SAPLING -> true;
            case OAK_SAPLING -> true;
            case MANGROVE_PROPAGULE -> true;
            case GRASS -> true;
            case TALL_GRASS -> true;
            case GRASS_BLOCK -> true;
            case WHEAT -> true;
            case BEETROOTS -> true;
            case POTATOES -> true;
            case CARROTS -> true;
            case PUMPKIN_STEM -> true;
            case MELON_STEM -> true;
            case FERN -> true;
            case LARGE_FERN -> true;
            case SWEET_BERRY_BUSH -> true;
            case KELP -> true;
            case KELP_PLANT -> true;
            case TALL_SEAGRASS -> true;
            case BAMBOO -> true;
            case CACTUS -> true;
            case SUGAR_CANE -> true;
            case COCOA -> true;
            default -> false;
        };
    }
}

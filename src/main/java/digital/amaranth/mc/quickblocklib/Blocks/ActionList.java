/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Blocks;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;

/**
 * a class for taking a repeating, scheduled, ordered action on a list of Blocks;
 * @author goodkinghenry
 */
public final class ActionList {
    private final List<Block> la, lb;
    
    private final Comparator<? super Block> s;
    private final Consumer<? super Block> a;
    
    private int taskID;
    private final Plugin p;
    private final long t;
    
    private boolean toggle;
    
    private void process () {
        if (toggle) {
            lb.stream().sorted(s).forEachOrdered(a);
            
            lb.clear();
        } else {
            la.stream().sorted(s).forEachOrdered(a);
            
            la.clear();
        }
        
        toggle = !toggle;
    }
    
    /**
     * start the ActionList's repeating schedule
     * @param d the number of ticks to delay before starting
     */
    public final void start (long d) {
        if (!Bukkit.getServer().getScheduler().isCurrentlyRunning(taskID)) {
            taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(p, () -> {
                process();
            }, d, t);
        } else {
            stop();
            start(d);
        }
    }
    
    /**
     * stop the ActionList's repeating schedule
     */
    public final void stop () {
        Bukkit.getServer().getScheduler().cancelTask(taskID);
        
        taskID = -1;
    }
    
    /**
     * @param b the Block which is to be processed at the next cycle
     */
    public final void addBlock (Block b) {
        if (toggle) {
            if (!la.contains(b)) {
                la.add(b);
            }
        } else {
            if (!lb.contains(b)) {
                lb.add(b);
            }
        }
    }
    
    /**
     * @param b the Block which is to be removed from the ActionList for this cycle
     */
    public void removeBlock (Block b) {
        if (toggle) {
            la.remove(b);
        } else {
            lb.remove(b);
        }
    }
    
    /**
     * @return the actively-written List of Blocks
     */
    public List<Block> getBlocks () {
        if (toggle) {
            return la;
        } else {
            return lb;
        }
    }
    
    /**
     * @param s a Comparator for sorting the Blocks before processing them
     * @param a a Consumer with which to act upon each Block at each cycle
     * @param t the number of ticks per cycle
     * @param d the number of ticks to wait before starting
     * @param p the Plugin that owns this ActionList
     */
    public ActionList(
            Comparator<? super Block> s, 
            Consumer<? super Block> a,
            long t,
            long d,
            Plugin p
    ) {
        la = new ArrayList<>();
        lb = new ArrayList<>();
        
        this.s = s;
        this.a = a;
        this.t = t;
        this.p = p;
        
        toggle = false;
        
        start(d);
    }
}

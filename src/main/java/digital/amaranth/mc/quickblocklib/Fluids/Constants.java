/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Fluids;

/**
 * constants pertaining to fluids.
 * @author goodkinghenry
 */
public class Constants {
    /**
     * this is the lowest integer value that a water block will have while flowing horizontally; note: minecraft fluid levels go in reverse, with 0 being the highest.
     */
    public final static int LOWEST_FLOWING_WATER_LEVEL = 7;

    /**
     * this is the lowest integer value that a fluid block will have while flowing horizontally; note: minecraft fluid levels go in reverse, with 0 being the highest.
     */
    public final static int LOWEST_FLOWING_FLUID_LEVEL = LOWEST_FLOWING_WATER_LEVEL;

    /**
     * this is the lowest integer value that a fluid block will have while falling vertically; note: minecraft fluid levels go in reverse, with 0 being the highest.
     */
    public final static int LOWEST_FALLING_FLUID_LEVEL = 15;

    /**
     * this is the lowest integer value that a lava block will have while flowing horizontally; note: minecraft fluid levels go in reverse, with 0 being the highest.
     */
    public final static int LOWEST_OVERWORLD_FLOWING_LAVA_LEVEL = 3;
    
    /**
     * the number of ticks between fluid events
     */
    public final static long TICKS_PER_FLUID_UPDATE = 5L;
}

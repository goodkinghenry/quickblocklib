/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Fluids;

import java.util.Optional;

import static digital.amaranth.mc.quickblocklib.Blocks.Categories.isAnAquaticPlant;

import static digital.amaranth.mc.quickblocklib.Fluids.Constants.LOWEST_FALLING_FLUID_LEVEL;
import static digital.amaranth.mc.quickblocklib.Fluids.Constants.LOWEST_FLOWING_FLUID_LEVEL;
import static digital.amaranth.mc.quickblocklib.Fluids.Constants.LOWEST_FLOWING_WATER_LEVEL;
import static digital.amaranth.mc.quickblocklib.Fluids.Constants.LOWEST_OVERWORLD_FLOWING_LAVA_LEVEL;

import org.bukkit.Material;
import org.bukkit.World;

import org.bukkit.block.Block;
import org.bukkit.block.data.Levelled;
import org.bukkit.block.data.Waterlogged;

/**
 * methods for dealing with fluids.
 * @author goodkinghenry
 */
public class Fluids {
    /**
     * @param b any Block
     * @return an Optional Integer which will, if b is a fluid Block, a value:
     * equal to the lowest horizontally flowing fluid level permitted for b's type in b's current world
     */
    public static Optional<Integer> getLowestAllowedFlowingLevel (Block b) {
        switch (b.getType()) {
            case WATER -> {
                return Optional.of(LOWEST_FLOWING_WATER_LEVEL);
            }
            case LAVA -> {
                if (b.getWorld().getEnvironment().equals(World.Environment.NETHER)) {
                    return Optional.of(LOWEST_FLOWING_FLUID_LEVEL);
                } else {
                    return Optional.of(LOWEST_OVERWORLD_FLOWING_LAVA_LEVEL);
                }
            }
            default -> {
                return Optional.empty();
            }
        }
    }
    
    public static boolean isAFlowingFluidLevel (int l) {
        return LOWEST_FLOWING_FLUID_LEVEL >= l && l > 0;
    }
    
    public static boolean isAFallingFluidLevel (int l) {
        return LOWEST_FALLING_FLUID_LEVEL >= l && l > LOWEST_FLOWING_FLUID_LEVEL;
    }
    
    public static boolean isASourceFluidLevel (int l) {
        return l == 0;
    }
    
    public static boolean isAFluidLevel (int level) {
        return LOWEST_FALLING_FLUID_LEVEL >= level;
    }
    
    /**
     * @param b any Block
     * @return true if a fluid would flow into b
     */
    public static boolean canBeFlowedToward (Block b) {
        return b.isPassable() && !b.isLiquid() && !isAnAquaticPlant(b);
    }
    
    /**
     * @param to the Block which would be flowed toward
     * @param from the Block from which flow would occur
     * @return true if from is a fluid Block and to is capable of being flowed into
     */
    public static boolean canFlowToward (Block to, Block from) {
        return isAFluidBlock(from) && canBeFlowedToward(to);
    }
    
    /**
     * @param b any Block
     * @return true if b is a fluid source Block
     */
    public static boolean isASourceBlock (Block b) {
        return b.getBlockData() instanceof Levelled l && l.getLevel() == 0;
    }
    
    public static boolean isAFlowingBlock (Block b) {
        return b.isLiquid() && !isASourceBlock(b);
    }
    
    public static boolean isAWaterBlock (Block b) {
        return b.getType().equals(Material.WATER);
    }
    
    public static boolean isALavaBlock (Block b) {
        return b.getType().equals(Material.LAVA);
    }
    
    public static boolean isAFluidBlock (Block b) {
        return b.isLiquid();
    }
    
    public static boolean isALevelledBlock (Block b) {
        return b.getBlockData() instanceof Levelled;
    }
    
    /**
     * @param b any Block
     * @return true if this Block can be waterlogged
     */
    public static boolean isWaterloggable (Block b) {
        return b.getBlockData() instanceof Waterlogged;
    }
    
    public static boolean isWaterlogged (Block b) {
        if (b.getBlockData() instanceof Waterlogged wl) {
            return wl.isWaterlogged();
        } else {
            return false;
        }
    }
    
    /**
     * @param b any Block
     * @param v true or false: whether the Block will be waterlogged
     * @return true if successful, or false otherwise
     */
    public static boolean setWaterlogged (Block b, boolean v) {
        if (b.getBlockData() instanceof Waterlogged wl) {
            wl.setWaterlogged(v);
            
            b.setBlockData(wl);
            
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * @param a
     * @param b
     * @return true if Block a has a fuller fluid level than Block b
     */
    public static boolean isFullerThan (Block a, Block b) {
        return getFluidLevel(a).orElse(LOWEST_FALLING_FLUID_LEVEL) < getFluidLevel(b).orElse(LOWEST_FALLING_FLUID_LEVEL);
    }
    
    /**
     * @param b any Block
     * @param v the level to which b will be set
     * @return true if successful, or false otherwise
     */
    public static boolean setFluidLevel (Block b, int v) {
        if (b.getBlockData() instanceof Levelled l && isAFluidLevel(v)) {
            l.setLevel(v);

            b.setBlockData(l);

            return true;
        }
        
        return false;
    }
    
    /**
     * @param b
     * @return an empty optional if b is not a fluid; otherwise, an optional integer
     */
    public static Optional<Integer> getFluidLevel (Block b) {
        if (b.getBlockData() instanceof Levelled l) {
            return Optional.of(l.getLevel());
        } else {
            return Optional.empty();
        }
    }
}

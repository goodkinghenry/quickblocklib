/*
 * QuickBlockLib
 * Copyright (C) 2022 Bradley Proffit
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package digital.amaranth.mc.quickblocklib.Entities;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

/**
 * Switches for matching entities into categories.
 * @author goodkinghenry
 */
public class Categories {
    public static boolean isTNT (Entity e) {
        return switch (e.getType()) {
            case PRIMED_TNT -> true;
            case MINECART_TNT -> true;
            default -> false;
        };
    }
    
    public static boolean isAZombie (LivingEntity le) {
        return switch (le.getType()) {
            case ZOMBIE -> true;
            case ZOMBIFIED_PIGLIN -> true;
            case ZOMBIE_VILLAGER -> true;
            case HUSK -> true;
            case DROWNED -> true;
            default -> false;
        };
    }
}
